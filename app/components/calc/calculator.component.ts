import { Component } from '@angular/core';
import { CalculatorService } from '../../services/calculator.service'
import * as _ from 'lodash'


@Component({
    moduleId: module.id,
    selector: 'calculator',
    templateUrl: 'calculator.component.html'
})
export class CalculatorComponent {

    sourceCountries:Array<Object>
    targetCountries:Array<Object>
    currencyForCountries:Array<Object>

    country_in: any
    country_out: any
    currency_in: any
    currency_out: any

    amount_in: number
    amount_out: string

    available_in:Array<Object>
    available_out:Array<Object>

    rate: number
    super_fast_available: boolean
    is_inverse: boolean
    orderType: string
    loading: boolean

    constructor(private _calculatorService: CalculatorService){
        this.loading = true
        this.amount_in = 100
        this.orderType = 'fast'
        this._calculatorService
            .getSourceCountries()
                .subscribe(res => {
                    this.sourceCountries = res
                    var index = _.findIndex(this.sourceCountries, (item: any) =>{
                        return item.name == 'Wielka Brytania'
                    })
                    this.country_in = this.sourceCountries[index]
                    this.currency_in = this.country_in.default_currency || null
                    this.getDestinationsCountries()

                })
    }

    getDestinationsCountries () {
        this._calculatorService
            .getDestinationsCountries(this.country_in.id)
            .subscribe(res => {
                this.targetCountries = res

                var index = _.findIndex(this.targetCountries, (item: any) =>{
                    return item.name == 'Polska'
                })
                if (index == -1){
                    this.country_out = this.targetCountries[0]
                }else{
                    this.country_out = this.targetCountries[index]
                }
                this.getCurrencyForCountries(this.country_in.id, this.country_out.id)
            })
    }

    getCurrencyForCountries(currencyIn:number, currencyOut:number){
        this._calculatorService.getCurrencyForCountries(currencyIn, currencyOut)
            .subscribe(res => {
                this.currencyForCountries = res
                this.available_out = []
                _.forEach(this.currencyForCountries, (item: any) => {
                    this.available_out.push(item.currency_out)
                })
                this.changeCurrencyOut(this.available_out[0])
            }
        )
    }

    changeCountryIn (newVal: Object){
        this.loading = true
        this.country_in = newVal
        this.currency_in = this.country_in.default_currency || null
        this.getDestinationsCountries()
    }

    changeCountryOut (newVal: Object){
        this.loading = true
        this.country_out = newVal
        this.getCurrencyForCountries(this.country_in.id, this.country_out.id)
    }

    changeCurrencyOut (newVal:Object) {
        this.loading = true
        this.currency_out = newVal
        this.orderType = 'fast'
        var i = _.findIndex(this.currencyForCountries, (item: any) => {
            return item.currency_out.id = this.currency_out.id
        })
        if (i != -1){
            this.super_fast_available = (this.currencyForCountries[i] as any).super_fast_available
        }else{console.error('smt wrong in changeCurrencyOut()')}
        this.getExchangeRate()
    }

    getExchangeRate(){
        console.log('getExchangeRate currency_in', this.currency_in)
        console.log('getExchangeRate currency_out', this.currency_out)
        this._calculatorService.getExchangeRate(this.currency_in.name, this.currency_out.name)
            .subscribe(res => {
                console.log(res)
                this.rate = res.rate
                this.setAmountOut()
            })
    }

    setAmountOut(){
        if (!this.amount_in && this.amount_in != 0){
            this.amount_in = 0
        }
        if (this.amount_in && this.rate){
            this.amount_out = (!!this.is_inverse ? this.amount_in / this.rate : this.amount_in * this.rate).toFixed(2)
        }
        this.loading = false
    }

}