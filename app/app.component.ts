import { Component } from '@angular/core';
import { CalculatorComponent } from './components/calc/calculator.component'
import { CalculatorService } from './services/calculator.service'
import { JSONP_PROVIDERS } from '@angular/http'

@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: 'app.component.html',
    directives: [ CalculatorComponent ],
    providers: [ JSONP_PROVIDERS, CalculatorService ]
})
export class AppComponent { }
