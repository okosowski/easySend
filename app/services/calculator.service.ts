import { Injectable } from '@angular/core'
import { Http } from '@angular/http';
import 'rxjs/Rx'
import { Observable }     from 'rxjs/Observable'

@Injectable()
export class CalculatorService{
    baseUrl: string;

    constructor(private _http: Http){
        this.baseUrl = 'http://localhost:3066' //defined in /proxyHelper/node.js
        console.log('CalculatorServices init')
    }

    getSourceCountries(){
        return this._http
            .get(this.baseUrl + '/api/calculator/countries')
            .map(response => response.json())
            .catch(this.handleError)
    }

    getDestinationsCountries(sourceId: number){
        return this._http
            .get(this.baseUrl + '/api/calculator/countries/' + sourceId + '/destinations')
            .map(response => response.json())
            .catch(this.handleError)
    }

    getCurrencyForCountries(countryIn:number, countryOut:number){
        return this._http
            .get(this.baseUrl + '/api/calculator/currencies/'+ countryIn +'/' + countryOut)
            .map(response => response.json())
            .catch(this.handleError)
    }

    getExchangeRate(currencyIn: string, currencyOut: string){
        return this._http
            .get(this.baseUrl + '/api/calculator/exchange-rate/'+ currencyIn +'/' + currencyOut)
            .map(response => response.json())
            .catch(this.handleError)
    }

    private handleError (error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

}