/*
    this is simple proxy server
    every GET request called to localhost:3066/XXX
    get answer from https://www.easysend.pl:443/XXX
 */

var http = require('http');
var https = require('https');

var express = require('express');
var app = express();

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.use(request)

app.listen(3066)

function request(cReq, cRes) {
  var options = {
    hostname: 'www.easysend.pl',
    port: 443,
    path: cReq.url,
    method: 'GET'
  };

  var proxy = https.request(options, function (res) {
    res.pipe(cRes, {
      end: true
    });
  });

  cReq.pipe(proxy, {
    end: true
  });
}
